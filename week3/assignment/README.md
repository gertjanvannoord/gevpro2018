Assignment
==========

In this assignment we will work towards a simple variant of
Conway's Game of Life. Zie: http://en.wikipedia.org/wiki/Conway's_Game_of_Life

Ensure you have the latest version of the assignment!

    git pull



Step 1: Reading a pattern as a list of coordinates (3 punten)
-------------------------------------------------------------

De input bestaat uit een aantal regels, en elke regel bevat spaties
en sterretjes. Voorbeeld:


...*...*...
..***.***..
.**.***.**. 


Het is de bedoeling dat je zo'n patroon omzet in een lijst
coordinaten. De eerste regel definieert de coordinaten (4,1) en
(8,1) omdat op plek 4 en 8 een sterretje staat, en omdat we de
eerste regel als y-coordinaat=1 beschouwen. De tweede regel
geeft coordinaten (3,2), (4,2), (5,2), (7,2), (8,2) en (9,2).
Etcetera.

Schrijf een functie read_pattern(bestand) die voor een gegeven 
bestand de lijst coordinaten retourneert. Een coordinaat is een 
tuple met twee elementen.

Hier zijn nog een paar voorbeelden om mee te testen:

     ....**......**....
     ...*.*......*.*...
     ...*..........*...
     **.*..........*.**
     **.*.*..**..*.*.**
     ...*.*.*..*.*.*...
     ...*.*.*..*.*.*...
     **.*.*..**..*.*.**
     **.*..........*.**
     ...*..........*...
     ...*.*......*.*...
     ....**......**....



     .................................*.
     ................*...............*.*
     ......*.*......*.....**........*...
     ......*....*....*.******....**.....
     ......*.********..........*..*.***.
     .........*.....*.......****....***.
     ....**.................***.*.......
     .*..**.......**........**..........
     .*..*..............................
     *..................................
     .*..*..............................
     .*..**.......**........**..........
     ....**.................***.*.......
     .........*.....*.......****....***.
     ......*.********..........*..*.***.
     ......*....*....*.******....**.....
     ......*.*......*.....**........*...
     ................*...............*.*
     .................................*.



     ..**......
     *..*......
     **.*.**...
     .*.*..*...
     .*....*.**
     ..***.*.**
     .....*....
     ....*.....
     ....**....



     .....*.....
     ....*.*....
     ...*.*.*...
     ...*...*...
     **.*.*.*.**
     *.*.....*.*
     ...*****...
     ...........
     .....*.....
     ....*.*....
     .....*.....


Even zoeken op Internet levert je heel veel (leuke!) patronen op. Of de
patronen leuk zijn of niet, weet je pas als je de hele opdracht tot een
goed einde hebt gebracht.


Step 2: Neighbors (twee punten)
-------------------------------

Schrijf een functie neighbors(coords) die voor een lijst coordinaten de 
coordinaten genereert van alle buren van de lijst. Voor een coordinaat zijn er
altijd acht buren. Er is dus geen sprake van een begrenzing van de
coordinaten. Die mogen kleiner dan 0 worden en ook vallen buiten de
oppervlakte van het begin-patroon.

We gebruiken een Counter om bij te houden hoevaak een coordinaat als
buur wordt gebruikt. De functie geeft als resultaat dus een Counter met
daarin alle gegenereerde coordinaten van de buren.

Step 3: Next Generation (twee punten)
-------------------------------------

Op basis van een lijst coordinaten ("de huidige wereld") en de in de
vorige opdracht gegenereerde Counter met buur-coordinaten kunnen we
nu een functie next_generation(coords,neighbors) bouwen die 
"de nieuwe wereld" construeert, ook dat is een lijst coordinaten. De
functie geeft dus een lijst coordinaten terug.

Dit zijn de regels:

- Als een positie minder dan twee keer in de buren datastructuur voorkomt, dan kun je deze positie overslaan.
- Als een positie precies twee keer als buur voorkomt, en de positie bestaat in de huidige wereld, dan bestaat de positie ook in de nieuwe wereld
- Als een positie precies drie keer als buur voorkomt, dan bestaat de positie in de nieuwe wereld
- Als een positie meer dan drie keer als buur voorkomt, dan kun je deze positie overslaan.


Step 4: Loop en Visualization (drie punten)
-------------------------------------------

Het spel bestaat eruit dat je steeds opnieuw een volgende wereld
genereert. Definieer hiertoe een loop, en zorg er daarbij voor dat
de nieuwe wereld steeds op het scherm wordt getoond.  Hoe je de
visualizatie wilt doen mag je zelf weten. Mooie oplossingen worden op
prijs gesteld en verdienen een hoger cijfer. 



Final step: Submit your work
----------------------------

Use "life.py" as the name of the file that contains your program.
Your program should take a single command line argument: the pattern
to start the game from. 

Ensure that you add life.py in your git repository, commit,
and push. If your solution uses any non-standard additional files,
please add these too (for instance graphics.py if you decide to use that).

    git push mine master

Go to https://bitbucket.org and check the files to verify all your
changes have arrived (you may have to login to BitBucket first).

Finally, submit via Nestor. Again, you do not have to attach any files, just
submit the URL of your private BitBucket repository.

