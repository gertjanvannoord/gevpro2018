Assignment
==========

In this assignment you will work with XML, JSON, strings and various data
structures.

Ensure you have the latest version of the assignment!

    git pull



Step 1: Finding Adjectives (4.5 Points)
----------------------------------

Cornetto is a digital lexical database for Dutch. It is available in
`/net/corpora/Cornetto2.0`. The assignment directory contains a small sample of
Cornetto’s word list in the file `cdb-sample.xml`.

Create a Python module `cdb` with a function `get_adjectives`. The function takes
the path to a Cornetto database XML file such as `cdb-sample.xml` or
`/net/corpora/Cornetto2.0/cdb2.0.id.xml`. It returns a collection of adjectives in
the database.

Requirements:

* Use `xml.etree.ElementTree` to read the XML file and iterate over its `cid`
  elements. Each element corresponds to a word.
* The returned collection is a collection of Python strings. It contains the
  values of the `form` attributes of `cid` elements.
* In the returned collection, include only adjectives. `cid` elements corresponding
  to adjectives have `ADJ` as the value of their `pos` (part of speech) attribute.
* Use a filtered list comprehension to filter out the non-adjectives.
* Some words have duplicate entries. Make sure the returned collection contains
  each adjective only once.
For next year: the function returns a set, cdb.py should define a main function too

Step 2: Solving Word Search Puzzles (4.5 Points)
------------------------------------------------

Create a module `wordsearch` with a function `solve`. The function takes the path
to a text file with a word search puzzle, e.g. `puzzle1.txt`:

    WMELKXIS
    WHWBRZDA
    SESIXUTN
    RIBAEHDI
    EFBCETKS
    TFFEHDLN
    AOSISSAC
    WKFQTSKQ

The function returns a list of all words hidden in rows (from left to right) and
columns (from top to bottom). For example, for the above puzzle, it should
return `['MELK', 'KLAK']`.

In order to find the words, your program needs a list of possible words.
Use the words in `words.json` for this (use the json module for this!).

Step 3: Advanced Word Search (1 Point)
--------------------------------------

Add a function `solve_advanced` to the `wordsearch` module. It should do the
same, but also find words that run backwards, i.e., from right to left or from
bottom to top. For the above puzzle, it should return
`['MELK', 'KLEM', 'CASSIS', 'KLAK', 'WATER', 'KOFFIE', 'KALK', 'INAS', 'SINAS']`.

No unit test is provided for the bonus step, you have to test it manually or by
writing your own unit test.

Step 4: Run the Unit Tests
--------------------------

From the current directory, run the following command:

    python3 -m unittest

If you completed steps 1–2, all tests should pass. Note that additional tests
may be used to grade your work.

For next year: add the name of the json file as function parameter, and add main function


Final step: Submit your work
----------------------------

For next year: check pep8/pycodestyle!

Run `git status` to see all the files you modified and created. There
should be two untracked files: `cdb.py` and `wordsearch.py`. `git add`
them. Commit the changes with a meaningful message.  Push them to your
private repository using this command:

    git push mine master

Go to https://bitbucket.org and check the files to verify all your
changes have arrived (you may have to login to BitBucket first).

Finally, submit via Nestor. Again, you do not have to attach any files, just
submit the URL of your private BitBucket repository.

