Assignment for Week 4
=====================

Get the Starter Code
--------------------

On the command line, go into your local copy of the course repository. Update
it using the following command.

    git pull


Problem: extract tokenized sentences from idiosyncratic text file
-----------------------------------------------------------------

The files vk???.txt.gz are gzip-compressed files which contain a
newspaper article each from "de Volkskrant", 2007. The format is
not quite standard, but if you inspect the file, you will quickly
understand it.  The goal of this week is to extract all sentences from
such a file in tokenized format. In the output, each line consists of
a tokenized sentence where all tokens are separated by a single
space.

As you can see in the example document, articles start with some
meta-information which we ignore for the purpose of this exercise. 
In the body of the article, paragraphs are easily
recognized because each paragraph starts on a new line. The
identification of sentences and tokens is much harder!

Examples of the required output for the vk???.txt.gz files is provided
in the corresponding *.sents files.

NOTE: it is quite hard to build a sentence splitter / tokenizer which *precisely* 
reconstructs the *.sents files. We will evaluate your solution by comparing it
on a similar but different part of Volkskrant data. More differences with our
gold standard will result in a lower grade! 

Your program extract_sents.py should take a single file name argument, which is the compressed
input file. It should write to standard output the resulting sentences. Thus,
we will run your program as follows:

    python3 extract_sents.py vk003.txt.gz > output.sents
    diff output.sents vk003.sents

Here, *diff* is the standard Unix command which shows the differences between
two similar files.

Final: Submit Your Work
------------------------

From the `week4/assignment` directory, run `git status` to see all the files
you modified and created. Add them all and commit the changes with a
meaningful message. Push them to your private repository using this command:

    git push mine master

Go to https://bitbucket.org/USERNAME/gevpro2018/src/master/week4/assignment/
(substituting your own username) and check the files to verify all your
changes have arrived (you may have to login to BitBucket first). Ensure
that the teacher and the teaching assistent(s) have access to your files.

Finally, submit the URL of your private BitBucket repository via Nestor. 

