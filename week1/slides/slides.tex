\documentclass{beamer}

%\usetheme{Singapore}

\usepackage{courier}
\usepackage{fancyvrb}
\usepackage{listings}
\usepackage{relsize}

\newcommand{\ccft}[3]{
\usebeamercolor{#1}
\definecolor{#2}{named}{fg}
\definecolor{#3}{named}{bg}
}

\ccft{block title}{myblock title fg}{myblock title bg}
\ccft{block body}{myblock body fg}{myblock body bg}
\ccft{block title alerted}{myblock title alerted fg}{myblock title alerted bg}
\ccft{palette secondary}{palette secondary fg}{palette secondary bg}

\definecolor{lightblue}{rgb}{0.9,0.9,1}
\lstset{basicstyle=\ttfamily, showstringspaces=false, backgroundcolor=\color{lightblue}, keywordstyle={\color{blue}\textbf}}

\title{Introduction to Software Development in Teams}
\author{slides: Kilian Evang\\
\quad\\
Gevorderd programmeren\\
Week 1, Lecture}
\date{}

\begin{document}

\begin{frame}
 \titlepage
\end{frame}

%\section*{Outline}

\begin{frame}
 \tableofcontents
\end{frame}

\section[Teams]{Software Development in Teams}

\frame{\tableofcontents[currentsection]}

\subsection[Teams]{}

\begin{frame}
 \frametitle{Software Development in Teams}
 \begin{itemize}
  \item Communication is key!
  \item Teams need to agree on:
  \begin{enumerate}
   \item functional requirements
   \item nonfunctional requirements
   \item style conventions
   \item interfaces and contracts
   \item how to exchange code
  \end{enumerate}
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Things to Agree on (1): Functional Requirements}
 \begin{itemize}
  \item Functional requirements = what should the program do? What are its capabilities?
  \item E.g. ``user enters a number $b$ and an exponent $n$, the program outputs the result of $b^n$''
  \item E.g. ``afterwards, it asks the user whether to calculate another power''
  \item Functional requirements often given by customer or teacher
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Things to Agree on (2): Nonfunctional requirements}
 \begin{itemize}
  \item Nonfunctional requirements = how should the program be structured internally? What is its architecture?
  \item E.g. ``program uses a function called \lstinline+power+ which takes two arguments: 1) the base $b$ and 2) the exponent $n$; it returns the value of $b^n$''
  \item E.g. ``the program should consist of distinct components model, view, controller'' (week 2)
  \item Nonfunctional requirements often given by lead developer or teacher
  \item UML diagrams can be used to make the architecture clear before starting to code (week 2)
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Interfaces as Contracts}
 %\smaller\smaller
 \begin{itemize}
  \item Person who \emph{writes} a component should clearly communicate to
        the person who \emph{uses} a component \emph{how} to use it and what to
        expect.
  \item I.e.: What classes and methods are available, with what arguments
        they should be called, and what they do (interface).
  \item Such information should go into docstrings. E.g.:
        \begin{lstlisting}
"""
The method power takes two numerical
arguments: a base b and the exponent n. It
returns b taken to the n-th power.
"""\end{lstlisting}
  \item It's like a \emph{contract}:
  \begin{itemize}
   \item ``If you promise that you will always call my method with the correct
         arguments, I promise that you will get the correct answer.''
  \end{itemize}
 \end{itemize}
 \larger\larger
\end{frame}

\begin{frame}
 \frametitle{Unit Tests}
 \begin{itemize}
  \item Does the program adhere to functional, non-functional requirements? Does it fulfill the contract?
  \item Define \emph{test cases}, check automatically if the program behaves as expected
  \item Cannot find \emph{all} bugs, but many
  \item Useful for checking you didn't break the code after making changes
  \item Will be used for grading your homework
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Defining a Unit Test}
 File \lstinline+test_power.py+:
 \begin{lstlisting}[language=python]
import power
import unittest

class PowerTest(unittest.TestCase):

    def test_power(self):
        self.assertEqual(power.power(2, 7),
                         128)
        self.assertEqual(power.power(3, 2), 9)
 \end{lstlisting}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Running Unit Tests}
 \begin{lstlisting}
$ python3 -m unittest
 \end{lstlisting}
 \begin{itemize}
  \item Runs all tests in files \lstinline+test_*.py+ in current directory
  \item Tells you which tests succeed and which fail, and why
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Cheating on Unit Tests?}
 \begin{lstlisting}[language=python]
def power(base, exponent):
    if base == 2 and exponent == 7:
        return 128
    if base == 3 and exponent == 2:
        return 9
 \end{lstlisting}
 \begin{itemize}
  \item Program passes test but does not fulfill contract
  \item Easily caught by using additional test cases
  \item Don't do this
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{A correct and properly documented version}
 \begin{lstlisting}[language=python]
def power(base, exponent):
    """
    The method power takes two numerical
    arguments: a base b and the exponent n. It
    returns b taken to the n-th power.
    """
    return base ** exponent
 \end{lstlisting}
 Learn more about docstrings: \url{http://www.pythonforbeginners.com/basics/python-docstrings}
\end{frame}

\begin{frame}
 \frametitle{Things to Agree on (3): Division of Labor}
 \begin{itemize}
  \item Programs should be modular.
  \item Modularity enables clear division of labor -- each team member can be
        mainly responsible for one or more components.
  \item E.g., a team of three with one responsible for model component, one for
        view component, one for controller component.
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Things to Agree on (4): Style Conventions}
 \begin{itemize}
  \item The coding style should be consisent within one project.
  \item E.g.: should attribute and method names use under\_scores or camelCase?
  \item E.g.: how many spaces to use for indentation? (4.)
  \item For this course, use this style guide (Python de-facto standard):
        \url{https://www.python.org/dev/peps/pep-0008/}
  \item \texttt{pep8} command-line tool can partially check style automatically
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Things to Agree on (5): Exchanging Code}
 How do team members get the code each other has written?
 \begin{itemize}
  \item Send them around via email? Horrible, messy.
  \item Dropbox or similar? No good either:
  \begin{itemize}
   \item Alice saves a file in the middle of something, suddenly Bob can't run the program anymore.
   \item Alice and Bob edit a file at the same time, chaos ensues.
   \item Very hard to find out who did what when, and why.
  \end{itemize}
  \item No excuse for not using a \emph{version control system} (VCS)!
 \end{itemize}
\end{frame}

\section[Git]{Version Control with Git}

\frame{\tableofcontents[currentsection]}

\subsection{Version Control Systems}

\begin{frame}
 \frametitle{What Are Version Control Systems?}
 Version control systems (VCSs) are essential tools for collaborative
 development of software (and documentation, etc.). They
 \begin{itemize}
  \item allow multiple people to work on the same files/directories without
        stepping on each other's toes
  \item keep a full history of all changes (similar to Wikipedia page history)
  \item make it possible to look up who changed what when, and why
  \item make it possible to revert bad changes
  \item ...
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Repositories and Revisions}
 \begin{center}
  \includegraphics[height=4cm]{ch02dia7}
 \end{center}
 \begin{itemize}
  \item a \emph{repository} is a directory shared between a team whose content changes over time
  \item the state of the repository at a given time is called a \emph{revision}
 \end{itemize}

 \tiny{Diagram from The SVN Book}
\end{frame}

\begin{frame}
 \frametitle{Centralized Setup}
 \begin{center}
  \includegraphics[width=5cm]{centralized}\\
 \end{center}
 \begin{itemize}
  \item One copy of the repository is kept on a server 
  \item Every team member has her/his own local copy of the repository
  \item Copies are kept in sync by periodically
  \begin{itemize}
   \item pushing your own changes to the server
   \item pulling others' changes from the server
  \end{itemize}
 \end{itemize}
 \tiny{Diagram from The SVN Book}
\end{frame}

%\begin{frame}
% \frametitle{File Servers vs. Centralized VCSs}
% \begin{itemize}
%  \item A centralized VCS allows all team members to upload and download files, like an ordinary file server.
%  \item In addition, it keeps a history.
%  \item In addition, it provides a mechanism for properly handling \emph{concurrent modification} -- i.e. two team members modifying the same file at the same time.
% \end{itemize}
%\end{frame}
%
%\begin{frame}
% \frametitle{Concurrent Modification: The Problem to Avoid}
% \begin{center}
%  \includegraphics[height=6cm]{ch02dia2}
% \end{center}
%
% \tiny{Diagram from The SVN Book}
%\end{frame}
%
%\begin{frame}
% \frametitle{Concurrent Modification: The Copy-Modify-Merge Solution}
% \begin{center}
%  \includegraphics[height=6cm]{ch02dia4}
% \end{center}
%
% \tiny{Diagram from The SVN Book}
%\end{frame}
%
%\begin{frame}
% \frametitle{Concurrent Modification: The Copy-Modify-Merge Solution (cont.)}
% \begin{center}
%  \includegraphics[height=6cm]{ch02dia5}
% \end{center}
%
% \tiny{Diagram from The SVN Book}
%\end{frame}

\begin{frame}[fragile]
 \frametitle{Git}
 \begin{itemize}
  \item Git: the most widely used VCS
  \item Free, open source
  \item Most important commands: status, add, commit, pull, push
  \item \url{https://git-scm.com/}
  \item Tutorial: \url{https://try.github.io/}
  \item Will be used for all your homework assignments, group project
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{A Simple Git Workflow}
 \begin{enumerate}
  \item \emph{Initialize} a repository or \emph{clone} an existing one.
  \item Make changes
  \item \emph{Stage} changes for commit
  \item \emph{Commit} the added changes with a message
  \item \emph{Pull} others' changes from the origin, (resolve conflicts, commit
        the resolution)
  \item \emph{Push} your commits to a server so others can pull them
  \item Repeat from 2
 \end{enumerate}
\end{frame}

\subsection{Git Workflow in Detail}

\begin{frame}[fragile]
 \frametitle{Step 1: Clone a Repository from a Server}
 \begin{lstlisting}
$ git clone https://alice@bitbucket.org/alice/buffet.git\footnotesize
Cloning into 'buffet'...
remote: Counting objects: 3, done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
Checking connectivity... done.
 \end{lstlisting}
\end{frame}

%\begin{frame}
% \frametitle{Aside: Repositories}
% \begin{itemize}
%  \item A Git \emph{repository} contains the files belonging to a project,
%        including the history with earlier versions
%  \item Every team member has their own copy of the repository, or even more
%        (e.g. one on university computer, one on laptop)
%  \item One central copy on a server is usually used to exchange codes between
%        team members
% \end{itemize}
%\end{frame}

\begin{frame}
 \frametitle{Step 2: Make Changes}
 \begin{itemize}
  \item Create and modify files as you normally would
  \item E.g.: Add more information to README.md, add script.py
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{\texttt{git status}}
 \smaller\smaller
 \begin{lstlisting}
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   README.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	script.py

no changes added to commit (use "git add" and/or "git commit -a")
 \end{lstlisting}
 \begin{itemize}
  \item \texttt{git status} is your friend. Use this command frequently to see the
 status of your repository.
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Step 3: Stage Changes}
 \begin{lstlisting}
$ git add README.md
$ git add script.py
 \end{lstlisting}
 \begin{itemize}
  \item To stage new/modified files, use \lstinline+git add+
  \item To remove/rename tracked files, use \lstinline+git rm+/\lstinline+git mv+
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{\texttt{git status}}
 \begin{lstlisting}
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	modified:   README.md
	new file:   script.py

 \end{lstlisting}
\end{frame}

\begin{frame}[fragile]
 \frametitle{See what you staged}
 \begin{lstlisting}
$ git diff --cached
diff --git a/README.md b/README.md
index ec91dae..9c84284 100644
--- a/README.md
+++ b/README.md
@@ -1 +1,3 @@
 buffet
 ======
+
+A repository full of delicious foods
 \end{lstlisting}
 ...
 \begin{itemize}
  \item \texttt{git diff} shows you more detail than \texttt{git status}: changes inside files
  \item Note: to see changes you did \emph{not} yet stage, leave out \texttt{--cached}.
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Step 4: Commit Changes}
 \smaller\smaller
 \begin{lstlisting}
$ git commit -m "Added description to README, added script"
[master 9c84284] Added description to README, added script
 2 files changed, 6 insertions(+)
 create mode 100644 script.py
 \end{lstlisting}
\end{frame}

\begin{frame}
 \frametitle{Commits}
 \begin{itemize}
  \item Each commit should represent one logical step in the development
        of your application, e.g. fix \emph{one} bug or add \emph{one} new
        feature
  \item If you commit too rarely, others work with outdated code, risk of
        merge conflicts increases
  \item If you commit before you are finished with one task, others may work
        with intermediate, non-working code
  \item Commit messages are important! They tell team members what each commit does
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{\texttt{git status}}
 \begin{lstlisting}
$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

nothing to commit, working directory clean
 \end{lstlisting}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Step 5: Pull from Remote Repository}
 \begin{lstlisting}
$ git pull
remote: Counting objects: 5, done.
remote: Compressing objects: 100% (5/5), done.
remote: Total 5 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (5/5), done.
From https://bitbucket.org/kevang/buffet
   b5f8cf6..498a504  master     -> origin/master
Merge made by the 'recursive' strategy.
 logo.jpg | Bin 0 -> 13509 bytes
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 logo.jpg
 \end{lstlisting}
 \begin{itemize}
  \item Another team member added a logo while we were working!
  \item Git automatically merges the changes.
  \item If Git cannot merge automatically, it will ask you to do so manually
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{\texttt{git log}}
 \smaller\smaller\smaller
 \begin{lstlisting}
$ git log
commit bd7c030a2fc49ed10a123a90cc70a483be0b7c15
Merge: 9c84284 498a504
Author: Alice <alice@example.com>
Date:   Fri Jan 15 15:01:36 2016 +0100

    Merge branch 'master' of https://bitbucket.org/alice/buffet

commit 9c8428412061775ca7dcb1f58c49e8f590d691b2
Author: Alice <alice@example.com>
Date:   Fri Jan 15 15:00:57 2016 +0100

    Added description to README, added script

commit f5698ed8b52089aee040d968c008c2e6858d0e9e
Author: Bob <bob@example.com>
Date:   Fri Jan 15 14:52:18 2016 +0100

    Added logo

commit ec91daed05da85834bbf0ec180d58bed941892b6
Author: Alice <alice@example.com>
Date:   Fri Jan 15 11:59:23 2016 +0100

    Initial commit with contributors
 \end{lstlisting}
 \larger\larger
 \begin{itemize}
  \item \texttt{git log} shows you the history: who did what when?
  \item Use \texttt{git log --name-status} for details about file changes
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Step 6: Push Your New Commits to Server}
 \begin{lstlisting}
$ git push
Password for 'https://alice@bitbucket.org': 
Counting objects: 9, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (6/6), done.
Writing objects: 100% (6/6), 812 bytes | 0 bytes/s, done.
Total 6 (delta 0), reused 0 (delta 0)
To https://alice@bitbucket.org/alice/buffet.git
   498a504..bd7c030  master -> master
 \end{lstlisting}
 \begin{itemize}
  \item Now other team members can pull them
 \end{itemize}
\end{frame}

\section{Resolving Conflicts}

\frame{\tableofcontents[currentsection]}

\subsection{Introduction}

\begin{frame}
 \frametitle{Resolving Conflicts}
 \begin{itemize}
  \item{Concurrent modification in Git}
  \begin{itemize}
   \item you work on a file in your working directory 
   \item in the meantime, a coworker pushes a new version of this file to
         the server
   \item before you can push your version, you have to pull
   \item pull creates a merged version
  \end{itemize}
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Resolving Conflicts (cont.)}
 \begin{itemize}
  \item{Creating a merged version}
  \begin{itemize}
   \item the challenge: put your local changes and your colleague's changes in
         the repository together into one file
   \item if changes are in different parts of the file, Git does this
         automatically
   \item if changes are close together, manual intervention may be required --
         this situation is called a \emph{conflict}
  \end{itemize}
 \end{itemize}
\end{frame}

\subsection{An Example}

\begin{frame}[fragile]
 \frametitle{Conflicts: an Example}
 Suppose Jim and Sally are using Git to make a Sandwich together.
 They both check out this initial version of \texttt{sandwich.txt}:
 \begin{lstlisting}
Top piece of bread
Mayonnaise
Lettuce
Tomato
Provolone
Creole Mustard
Bottom piece of bread
 \end{lstlisting}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Conflicts: an Example (cont.)}
 Jim adds some toppings in his working copy:
 \begin{lstlisting}[escapechar=+]
Top piece of bread
Mayonnaise
Lettuce
Tomato
Provolone
+\textcolor{red}{Salami}+
+\textcolor{red}{Mortadella}+
+\textcolor{red}{Prosciutto}+
Creole Mustard
Bottom piece of bread
 \end{lstlisting}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Conflicts: an Example (cont.)}
 Meanwhile, Sally adds some other toppings in her working copy:
 \begin{lstlisting}[escapechar=+]
Top piece of bread
Mayonnaise
Lettuce
Tomato
Provolone
+\textcolor{blue}{Sauerkraut}+
+\textcolor{blue}{Grilled chicken}+
Creole Mustard
Bottom piece of bread
 \end{lstlisting}
\end{frame}

\begin{frame}
 \frametitle{Conflicts: an Example (cont.)}
 Then this happens:
 \includegraphics[height=6cm]{cartoon}\\
 \tiny{Cartoon: \url{http://geek-and-poke.com}, CC-BY}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Conflicts: an Example (cont.)}
 \begin{itemize}
  \item Sally was the first to push her version.
  \item Jim will have to resolve the conflict.
  \item When he pulls, he sees this:
 \end{itemize}
 \smaller\smaller
 \begin{verbatim}
$ git pull
Auto-merging sandwich.txt
CONFLICT (content): Merge conflict in sandwich.txt
Automatic merge failed; fix conflicts and then commit the
result.
 \end{verbatim}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Resolving a Conflict}
 The file with the conflict now contains \emph{conflict markers}. In its
 present state, it is not usable:
\smaller\smaller
 \begin{lstlisting}[escapechar=+]
Top piece of bread
Mayonnaise
Lettuce
Tomato
Provolone
<<<<<<< HEAD
+\textcolor{red}{Salami}+
+\textcolor{red}{Mortadella}+
+\textcolor{red}{Prosciutto}+
=======
+\textcolor{blue}{Sauerkraut}+
+\textcolor{blue}{Grilled chicken}+
>>>>>>> f14d784ad0f0dd9cf2cc5bdf5db92136d4468512
Creole Mustard
Bottom piece of bread
 \end{lstlisting}
\end{frame}

\begin{frame}
 \frametitle{Resolving a Conflict (cont.)}
 \begin{itemize}
  \item Remember: communication is key!
  \item Team members need to agree on how a conflict should be resolved.
  \item Sometimes it's obvious.
  \item Sometimes, now you need to discuss.
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Resolving a Conflict (cont.)}
 \begin{itemize}
  \item Jim needs to talk to Sally about what to put on the sandwich.
  \item All off their toppings? Might be a bit much.
  \item Also, you won't get sauerkraut at their favorite Italian deli.
  \item How about a compromise: prosciutto and grilled chicken, discard the other additions.
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Resolving a Conflict (cont.)}
 After reaching an agreement with Sally, Jim edits the file and -- importantly
 -- removes the conflict markers:
 \begin{lstlisting}[escapechar=+]
Top piece of bread
Mayonnaise
Lettuce
Tomato
Provolone
+\textcolor{green}{Prosciutto}+
+\textcolor{green}{Grilled Chicken}+
Creole Mustard
Bottom piece of bread
 \end{lstlisting}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Resolving a Conflict (cont.)}
 Finally, he tells Git that the conflict is resolved by adding the
 manually merged file. Then, commit and push as usual.
\smaller\smaller
 \begin{lstlisting}
$ git add sandwich.txt
$ git commit -m "Compromise sandwich agreed on with Sally."
$ git push
...
 \end{lstlisting}
\larger\larger
\end{frame}


\section{This course}

\frame{\tableofcontents[currentsection]}

\begin{frame}
\frametitle{All materials are on git too}
\begin{itemize}
\item Sheets
\item Exercises
\item Studiehandleidings
\item Project
\end{itemize}

Note:

\begin{itemize}
\item Most of the material is by Kilian Evang
\item Most of the material is out of date
\end{itemize}


\end{frame}

\begin{frame}[fragile]
\frametitle{Getting Started}

{
\small
\begin{lstlisting}
git clone https://NAME@bitbucket.org/NAME/gevpro2018.git
\end{lstlisting}
}

where NAME is \verb+gertjanvannoord+


{
\tiny
\begin{lstlisting}
git clone https://gertjanvannoord@bitbucket.org/gertjanvannoord/gevpro2018.git
\end{lstlisting}
}

\begin{itemize}
\item You get a local copy of all course materials
\item Often do a \verb+git pull+ to ensure you have the latest additions and changes
\item the first assignment is in week1/assignment/
\end{itemize}


\end{frame}






\end{document}
